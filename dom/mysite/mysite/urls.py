from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^blog/', include('microblog.urls')),
    url(r'^admin/', include(admin.site.urls))
)
#handler404 = 'mysite.views.error404_view'
#handler500 = 'microblog.views.error500_view'