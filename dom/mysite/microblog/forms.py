from django import forms


class AddForm(forms.Form):
    title = forms.CharField(min_length=5, max_length=100)
    content = forms.CharField(min_length=10, max_length=200, widget=forms.Textarea)
    author = forms.CharField(max_length=200, widget=forms.HiddenInput)


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=50)
    passs = forms.CharField(max_length=50, widget=forms.PasswordInput)
    email = forms.EmailField(max_length=50)


class LoginForm(forms.Form):
    login = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)


class TagForm(forms.Form):
    name = forms.CharField(max_length=20, min_length=2)