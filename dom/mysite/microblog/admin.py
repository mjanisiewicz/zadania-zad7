from django.db import models
from models import Entry, Tag
from django.utils import timezone
from django.contrib import admin
# Create your models here.

class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'published')
    ordering = ('published', )
    search_fields = ('tags__name', )

class TagAdmin(admin.ModelAdmin):
    list_display = ('name', )

admin.site.register(Entry, EntryAdmin)
admin.site.register(Tag, TagAdmin)