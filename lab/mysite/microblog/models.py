from django.db import models
from django.utils import timezone
# Create your models here.
class Entry(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=500)
    published = models.DateTimeField('dodano')
    nick = models.CharField(max_length=200)

    def __unicode__(self):
        return self.title

    def published_today(self):
        now = timezone.now()
        time_delta = now - self.pub_date
        return time_delta.days == 0
