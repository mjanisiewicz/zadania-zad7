# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib import messages

from models import Entry
from django.utils import timezone

def add_entry_view(request):
    if request.method == "POST":
        title = request.POST.get('title', 0)
        content = request.POST.get('content', 0)
        nick = request.POST.get('nick', 0)

        date = timezone.now()
        entry = Entry(title=title, content=content, nick=nick, published=date)
        entry.full_clean()
        entry.save()
        url = reverse('list_entries')
    else:
        url = reverse('add_entry_get')
    return HttpResponseRedirect(redirect_to=url)