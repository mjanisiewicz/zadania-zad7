from django.conf.urls import patterns, url
from django.views.generic import ListView, TemplateView

from models import Entry
import views
urlpatterns = patterns('',
    url(r'^$', ListView.as_view(
        template_name='microblog/stub.html',
        queryset=Entry.objects.all(),
        context_object_name='entries',
    ), name='list_entries'),

    url(r'add$', views.add_entry_view, name='add_entry'),
    url(r'addentry$', TemplateView.as_view(
        template_name='microblog/add.html',
        ), name='add_entry_get'),
)
